module.exports = {
    "extends": "eslint-config-hapi",
    "plugins": [
        "eslint-plugin-hapi"
    ],
    rules: {
        'hapi/hapi-scope-start': 0
    }
};