'use strict';
require('dotenv').config();
const logger = require('bucker').createLogger({ console: { timestamp: 'MM/DD/YY HH:mm:ss' } });
require('./modules/gaming/history-service');
const Glue = require('glue');
const manifest = {
    server: { debug: { request: 'error' } },
    connections: [{ host: 'localhost',
        port: process.env.HAPI_PORT }],
        registrations: [
            {
                plugin: {
                    register: './authentication.js' // plugin to authenticate user
                }
            },
            {
                plugin:{
                    register: 'blipp'
                }
            },
            {
                plugin:{
                    register: 'hapi-ending',
                    options : {
                        baseUrl: 'http://www.bitcoin-trade-game.com/',
                        enabled: true,
                    }
                }
            },
            {
                plugin: {
                    register: './gaming/index.js'
                },
                options : {
                    routes: {
                        prefix: '/api'
                    }
                }
            }
        ]
};

const options = {
    relativeTo: __dirname + '/modules',
};

Glue.compose(manifest, options).then( (server) => {
    server.start( (err) => {
        if (err) {
            throw err;
        }
        logger.info(`Server running at: ${server.info.uri}`);
    });
});