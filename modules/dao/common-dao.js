'use strict';

require('dotenv').config();
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const url = process.env.DB_URL;
let db;

// Use connect method to connect to the server
const connect = () => {
    if (db) {
        return Promise.resolve(db);
    }
    return MongoClient.connect(url)
        .then( (database) => {
            db = database;
            return db;
        });
};
/**
 * Connect and get collection by collectionName
 * @param collectionName name of collection to work on
 */
const getCollection = (collectionName) => {
    return connect()
    .then((database) => database.collection(collectionName));
};

/**
 * Post a new document in collection
 * @param document
 * @param collectionName name of collection to insert
 * @returns {*}
 */
const save = (document, collectionName) => {
    return getCollection(collectionName)
    .then((collection) => collection.insertOne(document))
    .then((result) => result.ops[0]);
};

/**
 * Update an existing document in collection
 * @param document
 * @param collectionName name of collection to update
 * @returns {*}
 */
const update = (id, document, collectionName) => {
    document._id = new ObjectID(id);
    return getCollection(collectionName)
    .then((collection) => collection.replaceOne({ '_id': ObjectID(id) }, document))
    .then((result) => result.ops[0]);
};

/**
 * Remove a document by its id from the collection.
 * @param {String} id
 * @param {String} collectionName name of collection to remove document
 */
const remove = (id, collectionName) => {
    return getCollection(collectionName)
    .then((collection) => collection.deleteOne({ '_id': ObjectID(id) }));
};

/**
 * Find a document by id and collection
 * @returns {*}
 */
const findById = (id, collectionName) => {
    return getCollection(collectionName)
    .then((collection) => collection.findOne({ '_id': ObjectID(id) }));
};

exports.connect = connect;
exports.getCollection = getCollection;
exports.save = save;
exports.update = update;
exports.remove = remove;
exports.findById = findById;
