'use strict';

const CommonDAO = require('./common-dao');
const collectionName = 'histodaily';

const findAll = () => {
    return CommonDAO.getCollection(collectionName)
    .then((collection) => collection.find({}).toArray());
};

const findBySym = (sym) => {
    return CommonDAO.getCollection(collectionName)
    .then((collection) => collection.find({sym}).toArray());
}

const findByTime = (time) => {
  return CommonDAO.getCollection(collectionName)
  .then((collection) => collection.find({time}).toArray());
}

const findByTimeAndSym = (time, sym) => {
  return CommonDAO.getCollection(collectionName)
  .then((collection) => collection.findOne({time, sym}));
}

const findByTimeAndSyms = (time, syms) => {
  return CommonDAO.getCollection(collectionName)
  .then((collection) => collection.find({time, sym : { $in : syms}}).toArray());
}

const findAllTime = () => {
  return CommonDAO.getCollection(collectionName)
    .then((collection) => collection.distinct('time'));
}

const insertMany = (histodailyTab) => {
  return CommonDAO.getCollection(collectionName)
  .then((collection) => collection.insertMany(histodailyTab))
  .then((result) => result.ops[0]);
};

exports.save = (histo) => CommonDAO.save(histo, collectionName);
exports.update = (id, limit) => CommonDAO.update(id, limit, collectionName);
exports.remove = (id) => CommonDAO.remove(id, collectionName);
exports.findAll = findAll;
exports.findBySym = findBySym;
exports.insertMany = insertMany;
exports.findAllTime = findAllTime;
exports.findByTime = findByTime;
exports.findByTimeAndSym = findByTimeAndSym;
exports.findByTimeAndSyms = findByTimeAndSyms;
