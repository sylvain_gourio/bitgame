'use strict';
/**
 * account : {
 *  key,
 *  name:,
 *  initialBTC,
 *  BTC,
 *  wallets : [{
 *    currency:,
 *    balance:,
 *    costing:
 *  }]
 * }
 */

const CommonDAO = require('./common-dao');
const accountsCollectionName = 'accounts';

/**
 * Find a member by his key
 * @returns {*}
 */
const findByKey = (key) => {
    return CommonDAO.getCollection(accountsCollectionName)
    .then((collection) => collection.findOne({ key }));
};

const findAll = () => {
    return CommonDAO.getCollection(accountsCollectionName)
    .then((collection) => collection.find({}).toArray());
};

exports.save = (account) => CommonDAO.save(account, accountsCollectionName);
exports.update = (id, account) => CommonDAO.update(id, account, accountsCollectionName);
exports.findAll = findAll;
exports.findByKey = findByKey;
