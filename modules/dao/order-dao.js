'use strict';
/**
 * order : {
 *  key, // member key
 *  currency,
 *  orderType,
 *  quanity:,
 *  timeStamp,
 *  limit:,
 *  comission:,
 *  price:
 * }
 */

const CommonDAO = require('./common-dao');
const ordersCollectionName = 'orders';

const findById = (id) => {
    return CommonDAO.findById(id, ordersCollectionName);
};

/**
 * Find a member by his email
 * @returns {*}
 */
const findByKey = (key) => {
    return CommonDAO.getCollection(ordersCollectionName)
    .then((collection) => collection.find({ key }).toArray());
};

exports.save = (order) => CommonDAO.save(order, ordersCollectionName);
exports.findByKey = findByKey;
