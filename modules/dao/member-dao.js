'use strict';
/**
 * member : {
 *  email,
 *  displayName,
 *  creationDate,
 *  creationDateFr,
 *  key,
 *  secret
 * }
 */

const CommonDAO = require('./common-dao');
const membersCollectionName = 'members';

const save = (member) => {
    return CommonDAO.save(member, membersCollectionName);
};

const update = (id, member) => {
    return CommonDAO.update(id, member, membersCollectionName);
};

const findById = (id) => {
    return CommonDAO.findById(id, membersCollectionName);
};

/**
 * Find a member by his email
 * @returns {*}
 */
const findByEmail = (email) => {
    return CommonDAO.getCollection(membersCollectionName)
    .then((collection) => {
        return collection.findOne({ email });
    });
};

const findByKeyAndSecret = (key, secret) => {
    return CommonDAO.getCollection(membersCollectionName)
    .then((collection) => {
        return collection.findOne({ key, secret });
    });
};

/**
 * Find all members
 * @returns {*}
 */
const findAll = () => {
    return CommonDAO.getCollection(membersCollectionName)
    .then((collection) => {
        return collection.find({}).toArray();
    });
};

exports.save = save;
exports.update = update;
exports.findById = findById;
exports.findByEmail = findByEmail;
exports.findAll = findAll;
exports.findByKeyAndSecret = findByKeyAndSecret;
