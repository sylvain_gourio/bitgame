'use strict';
/**
 * account : {
 *  timestamp,
 *  key,
 *  name:,
 *  initialBTC,
 *  BTC,
 *  wallets : [{
 *    currency:,
 *    balance:,
 *    costing:
 *  }]
 * }
 */

const CommonDAO = require('./common-dao');
const historyCollectionName = 'history';

/**
 * Find a member by his email
 * @returns {*}
 */
const findByKey = (email) => {
    return CommonDAO.getCollection(historyCollectionName)
    .then((collection) => collection.find({ key }).toArray());
};

const findBeetweenDate = (start, end) => {
    return CommonDAO.getCollection(historyCollectionName)
    .then((collection) => collection.find({ 
        timestamp: {
        $gte: start,
        $lt: end
        }
    }).toArray());
}

const findAll = () => {
    return CommonDAO.getCollection(historyCollectionName)
    .then((collection) => collection.find({}).toArray());
};

exports.save = (account) => CommonDAO.save(account, historyCollectionName);
exports.update = (id, account) => CommonDAO.update(id, account, historyCollectionName);
exports.findAll = findAll;
exports.findByKey = findByKey;
exports.findBeetweenDate = findBeetweenDate;
