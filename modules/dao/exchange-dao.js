'use strict';
/**
 * {
 * fromSymbol
 * toSymbol
 * timestamp
 * markets : {
 *   marketName
 *   price
 * }
 * }
 */

const CommonDAO = require('./common-dao');
const collectionName = 'exchanges';

const findBeetweenDate = (currency, start, end) => {
    return CommonDAO.getCollection(collectionName)
    .then((collection) => collection.find({ 
        timestamp: {
        $gte: start,
        $lt: end
        },
        fromSymbol: currency
    }).toArray());
}

const findAll = () => {
    return CommonDAO.getCollection(collectionName)
    .then((collection) => collection.find({}).toArray());
};

const findForCurrency = (currency) => {
  return CommonDAO.getCollection(collectionName)
  .then((collection) => collection.find({fromSymbol : currency}).toArray());
}

exports.save = (exchange) => CommonDAO.save(exchange, collectionName);
exports.update = (id, exchange) => CommonDAO.update(id, exchange, collectionName);
exports.findAll = findAll;
exports.findBeetweenDate = findBeetweenDate;
exports.findForCurrency = findForCurrency;
