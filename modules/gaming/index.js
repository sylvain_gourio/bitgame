'use strict';
const Boom = require('boom');
const Joi = require('joi');
const logger = require('bucker').createLogger({ console: { timestamp: 'MM/DD/YY HH:mm:ss' } });
const AccountDAO = require('../dao/account-dao');
const MemberDAO = require('../dao/member-dao')
const OrderDAO = require('../dao/order-dao');
const HistoryDAO = require('../dao/history-dao');
const ExchangeDAO = require('../dao/exchange-dao');
const Bittrex = require('../bittrex/bittrex');
const GamingService = require('./gaming-service');
const FollowGainersSimulator = require('../simulators/scenarii/follow-gainers.js');
const SimulatorService = require('../simulators/simulator-service');
const Fees = 0.0025;

exports.register = function (server, options, next) {

    server.route([
        {
            path: '/accounts',
            method: 'POST',
            config: {
                description: 'Add a new account if not already exists',
                auth: {
                    strategy: 'session',
                    scope: ['member', 'admin']
                },
                handler: function (request, reply){
                    const account = request.payload;
                    account.BTC = account.initialBTC;
                    account.wallets = [];
                    return reply(
                        AccountDAO.findByKey(account.email)
                        .then( (acc) => {
                            return new Promise((resolve, reject) => {
                                if (acc) {
                                    return reject(Boom.badRequest('User already exists', acc));
                                }
                                return resolve();
                            });
                        }).then( () => AccountDAO.save(account))
                        .catch( (err) => {
                            err.output.payload.details = err.data;
                            return err;
                        })
                    );
                }
            }
        },
        {
            path: '/accounts',
            method: 'GET',
            config: {
                description: 'Get all existing accounts',
                auth: {
                    strategy: 'session',
                    scope: ['member', 'admin']
                },
                handler: function (request, reply){
                    return reply(
                        AccountDAO.findAll()
                    );
                }
            }
        },
        {
            path: '/getmarketsummaries',
            method: 'GET',
            config: {
                description: 'Get market summary',
                auth: {
                    strategy: 'session',
                    scope: ['member', 'admin']
                },
                handler: function (request, reply){
                    return reply(Bittrex.getMarketSummaries()
                    );
                }
            }
        },
        {
            path: '/accounts/mine',
            method: 'GET',
            config: {
                description: 'Get account by email',
                auth: {
                    strategy: 'session',
                    scope: ['member', 'admin']
                },
                handler: function (request, reply){
                    const credentials = request.auth.credentials;
                    return reply(
                        AccountDAO.findByKey(credentials.key)
                    );
                }
            }
        },
        {
            path: '/member/me',
            method: 'GET',
            config: {
                description: 'Get my info',
                auth: {
                    strategy: 'session',
                    scope: ['member', 'admin']
                },
                handler: function (request, reply){
                    const credentials = request.auth.credentials;
                    return reply(
                        MemberDAO.findByEmail(credentials.email)
                    );
                }
            }
        },
        {
            path: '/orders',
            method: 'POST',
            config: {
                description: 'Add a new order',
                auth: {
                    strategy: 'session',
                    scope: ['member', 'admin']
                },
                validate: {
                    payload : {
                        currency: Joi.string().required().description('Currency to buy or sell : "LTC", "BCC", "XMR", "DOGE", etc...'),
                        orderType: Joi.string().required().allow('BUY','SELL').description('Buy or sell order'),
                        quantity: Joi.number().required().description('Quantity of currency to buy or sell (integer)')
                    }
                },
                handler: function (request, reply){
                    const order = request.payload;
                    const credentials = request.auth.credentials;
                    return reply(
                        AccountDAO.findByKey(credentials.key)
                        .then((account) => {
                            if (!account) {
                                throw new Error('User with email ' + order.email + ' does not exist');
                            }
                            return Promise.all([account, Bittrex.getTicker(order.currency)]);
                        })
                        .then(([account, ticker]) => {
                            GamingService.processOrder(account, order, ticker);
                            return Promise.all([AccountDAO.update(account._id, account), OrderDAO.save(order)]);
                        })
                        .catch( (err) => {
                            logger.error(err);
                            return Boom.badRequest(err);
                        })
                    );
                }
            }
        },
        {
            path: '/orders/mine',
            method: 'GET',
            config: {
                description: 'Get my orders',
                auth: {
                    strategy: 'session',
                    scope: ['member', 'admin']
                },
                handler: function (request, reply){
                    const credentials = request.auth.credentials;
                    return reply(OrderDAO.findByKey(credentials.key));
                }
            }
        },
        {
            path: '/histories',
            method: 'GET',
            config: {
                description: 'Get histories',
                auth: {
                    strategy: 'session',
                    scope: ['member', 'admin']
                },
                handler: function (request, reply){
                    const params = request.query;
                    if(params.start && params.end){
                        return reply(HistoryDAO.findBeetweenDate(Number(params.start), Number(params.end)));
                    }
                    return reply(HistoryDAO.findAll());
                }
            }
        },
        {
            path: '/exchanges/{currency}',
            method: 'GET',
            config: {
                description: 'Get exchanges history for currency',
                auth: {
                    strategy: 'session',
                    scope: ['member', 'admin']
                },
                handler: function (request, reply){
                    const params = request.query;
                    if(params.start && params.end){
                        return reply(ExchangeDAO.findBeetweenDate(request.params.currency, Number(params.start), Number(params.end)));
                    }
                    return reply(ExchangeDAO.findForCurrency(request.params.currency));
                }
            }
        },
        {
            path: '/simulator/follow-gainers',
            method: 'GET',
            config: {
                description: 'Get the result of simulator follow gainers',
                auth: {
                    strategy: 'session',
                    scope: ['member', 'admin']
                },
                handler: function (request, reply){
                    const params = request.query;
                    if(params.start && params.end){
                        return reply(SimulatorService.run(Number(params.start),Number(params.end),FollowGainersSimulator.scenario(params.gainerFrom, params.gainerTo, params.reverse == 'true')));
                    }
                    return reply(SimulatorService.run(1518220800,1522540800,FollowGainersSimulator.scenario(0,10)));
                }
            }
        },
    ]);

    next();
};

exports.register.attributes = {
    pkg: require('./package.json')
};
