'use strict';
const Boom = require('boom');
const AccountDAO = require('../dao/account-dao');
const OrderDAO = require('../dao/order-dao');
const Fees = 0.0025;
const Moment = require('moment');


/**
 * Buy currency for an account
 * @param {*} account 
 * @param {*} order 
 * @param {*} ticker 
 */
const buy = (account, order, ticker) => {
    order.limit = ticker.Ask;
    order.price = (order.limit * order.quantity) + ((order.limit * order.quantity) * Fees);
    if (account.BTC < order.price) {
      throw new Error('Not enough BITCOIN, order cost is ' + order.price + 'BTC and only ' + account.BTC + 'BTC remaining.');
    }
    account.BTC = account.BTC - order.price;
    let w = account.wallets.find((wallet) => {
        return wallet.currency === order.currency;
    });
    if( !w ) {
        w = {
            currency : order.currency,
            costing : 0,
            quantity : 0
        };
        account.wallets.push(w);
    }
    w.costing = (order.price + w.costing * w.quantity) / (w.quantity + order.quantity);
    w.quantity = order.quantity + w.quantity;
};

/**
 * Sell currency for an account
 * @param {*} account 
 * @param {*} order 
 * @param {*} ticker 
 */
const sell = (account, order, ticker) => {
    const w = account.wallets.find((wallet) => {
      return wallet.currency === order.currency;
    });
    if (w.quantity < order.quantity){
      throw new Error('Not enough ' + order.currency + ', only ' + w.quantity + ' available.');    
    }
    order.limit = ticker.Bid;
    order.price = (order.limit * order.quantity) - ((order.limit * order.quantity) * Fees);
    w.quantity = w.quantity - order.quantity;
    account.BTC = account.BTC + order.price;
}

const processOrder = (account, order, ticker) => {
  order.timestamp = Date.now();
  order.creationDateFr = Moment(Date.now()).format('Do MMMM YYYY, HH:mm');
  order.key = account.key;
  if (order.orderType.toUpperCase() === 'BUY') {
    buy(account, order, ticker);
  } 
  else {
    sell(account, order, ticker);
  }
}

exports.processOrder = processOrder;
