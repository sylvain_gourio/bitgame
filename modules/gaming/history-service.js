'use strict';
const schedule = require('node-schedule');
const logger = require('bucker').createLogger({ console: { timestamp: 'MM/DD/YY HH:mm:ss' } });
const AccountDAO = require('../dao/account-dao');
const OrderDAO = require('../dao/order-dao');
const HistoryDAO = require('../dao/history-dao');
const ExchangeDAO = require('../dao/exchange-dao');
const Bittrex = require('../bittrex/bittrex');
const Cryptocompare = require('../cryptocompare/cryptocompare');
const Moment = require('moment');

const saveHistory = (now) => {
  return Promise.all([Bittrex.getMarketSummaries(), AccountDAO.findAll()])
    .then(([marketSummaries, accounts]) => accounts.map((account) => {
      account._id = undefined;
      account.timestamp = now;
      account.currentBTC = account.BTC
      + account.wallets.reduce((btc, wallet) => {
        const market = marketSummaries.find((market) => market.MarketName === 'BTC-' + wallet.currency);
        const last = market ? market.Last : 0;
        return btc + (last * wallet.quantity)
      }, 0);
      return account;
    }))
    .then((accounts) => {
      return accounts.forEach((account) => {
        HistoryDAO.save(account);
      });
    })
    .catch(err => logger.error(err));
}

const _saveExchange = (now, fromSymbol, toSymbol, markets) => {
  return Cryptocompare.getCoinSnapshots(fromSymbol, toSymbol)
  .then((response) => {
    const histo = {
      fromSymbol,
      toSymbol,
      timestamp: now,
      markets: []
    }
    markets.forEach((market) => {
      const exchange = response.Data.Exchanges.find(exchange => exchange.MARKET == market);
      if (exchange) {
        histo.markets.push({
          marketName: market,
          price: exchange.PRICE
        })
      }
    });
    return histo;
  })
  .then(histo => ExchangeDAO.save(histo))
  .catch(err => logger.error(err));
}

const saveExchangePlateformPrices = (now) => {
  const fromSymbol = 'BTC';
  const toSymbol = 'USD';
  const markets = ['Coinbase', 'Kraken', 'Bitfinex', 'BitTrex', 'Poloniex', 'HitBTC', 'Binance'];
  const currencies = ['XMR', 'BCH', 'XRP', 'LTC', 'IOT', 'ADA'];
  const promises = [_saveExchange(now, 'BTC', 'USD', markets)];
  currencies.forEach(currency => promises.push(_saveExchange(now, currency, 'BTC', markets)));
  return Promise.all(promises);
}


schedule.scheduleJob('30 * * * *', () => {
  logger.info('Execute "save history" task. ' + Moment(new Date()).format('Do MMMM YYYY, HH:mm'));
  saveHistory(Date.now());
});

schedule.scheduleJob('*/5 * * * *', () => {
  logger.info('Execute "saveExchangePlateformPrices" task. ' + Moment(new Date()).format('Do MMMM YYYY, HH:mm'));
  saveExchangePlateformPrices(Date.now());
});

exports.saveHistory = saveHistory;
exports.saveExchangePlateformPrices = saveExchangePlateformPrices;

saveExchangePlateformPrices(Date.now());
