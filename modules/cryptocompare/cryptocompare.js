'use strict';
const Fetch = require('node-fetch');
const Moment = require('moment');
Moment.locale('fr');

const getCoinSnapshots = (fromSymbol, toSymbol) => {
    return Fetch(`https://www.cryptocompare.com/api/data/coinsnapshot/?fsym=${fromSymbol}&tsym=${toSymbol}`)
    .then((response) => response.json());
};

exports.getCoinSnapshots = getCoinSnapshots;
