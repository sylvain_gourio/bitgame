'use strict';
const Fetch = require('node-fetch');
const Moment = require('moment');
Moment.locale('fr');

const getMarketSummaries = () => {
    return Fetch('https://bittrex.com/api/v1.1/public/getmarketsummaries')
    .then((response) => response.json())
    .then((response) => response.result);
};

const lastTicker = [];

const getTicker = (currency) => {
    if (lastTicker[currency] && Moment().subtract(1,'m').isBefore(lastTicker[currency].timestamp) ){
        return lastTicker[currency].ticker;
    }
    return Fetch('https://bittrex.com/api/v1.1/public/getticker?market=BTC-' + currency)
    .then((response) => response.json())
    .then((response) => {
        lastTicker[currency] = { 
            ticker: response.result,
            timestamp: new Date()
        }
        return response.result;
    });
};

exports.getTicker = getTicker;
exports.getMarketSummaries = getMarketSummaries;
