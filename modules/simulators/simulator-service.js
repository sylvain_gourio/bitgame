'use strict';

const run = async (start, end, scenarii) => {
  const snapshots = new Array();
  let snapshot = {
    btc : 1,
    wallets: [],
    time: start,
    estimatedValue: 1
  };
  snapshots.push(snapshot);
  if (start && end && end > start) {
    let current = start;
    console.time('scenario');
    while (current <= end) {
      snapshot = await scenarii(current, snapshot);
      snapshot.time = current;
      snapshots.push(snapshot);
      current = current + 86400
    }
    console.timeEnd('scenario');
  }
  return snapshots;
}

const test = (r) => (current, snapshot) => {
  return {
    btc: snapshot.btc - r,
    wallets: []
  }
}

const reti1 = test(1);

exports.run = run;
