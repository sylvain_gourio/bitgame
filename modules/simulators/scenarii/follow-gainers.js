'use strict';

const HistodailyDAO = require('../../dao/histodaily-dao');
const logger = require('bucker').createLogger({ console: { timestamp: 'MM/DD/YY HH:mm:ss' } });


const scenario = (gainerFrom = 1, gainerTo = 5, reverse = false) => async (time, previousSnapshot) => {
  try{
    const snapshot = {
      btc : previousSnapshot.btc,
    }
    snapshot.wallets = previousSnapshot.wallets.slice(0);
    snapshot.logs = new Array();
    
    // find yesterday gainers
    let gainers = await findGainers(time - 86400);
    if (reverse) {
      gainers = gainers.reverse();
    }
    const toBuy = gainers.slice(gainerFrom - 1, gainerTo);
    const histos = await HistodailyDAO.findByTime(time);
    // sold all non gainer coin
    const walletsToSell = snapshot.wallets.filter(wallet => !toBuy.map(gainer => gainer.sym).includes(wallet.sym));
    let result = await sellWallets(time, snapshot, walletsToSell, histos);

    // buy gainer
    const symToBuy = toBuy.filter((gainer) => !snapshot.wallets.map(wallet => wallet.sym).includes(gainer.sym));
    result = await buySym(time, result, symToBuy, histos);
    result = await valueSnaphot(time, result, histos);
    return result;
  } catch (e) {
    logger.error(e);
  }
}

const gainPercent = (histo) =>  (histo.close - histo.open) / histo.open * 100;

const findGainers = async (time) => {
  let histo = await HistodailyDAO.findByTime(time);
  histo = histo.filter(h => h.volumeto > 10)
          .sort((histo1, histo2) => gainPercent(histo2) - gainPercent(histo1));
  return histo;
}

const sellWallets = async(time, snapshot, walletsToSell, histos) => {
  const result = {...snapshot};
  for (const wallet of walletsToSell) {
    const histo = histos.find(histo => histo.sym === wallet.sym);
    const gain = wallet.quantity * histo.open;
    result.btc = precisionRound(result.btc + gain - (gain * 0.0025), 8);
    const index = result.wallets.indexOf(wallet);
    if (index >= 0) {
      result.wallets.splice(index, 1);
    }
    const plusvalue = precisionRound((histo.open - wallet.price) / histo.open * 100,2);
    snapshot.logs.push(`Sell ${wallet.quantity} ${wallet.sym} at price ${histo.open} => ${plusvalue}%`);
  };
  return result;
}

const buySym = async(time, snapshot, symToBuy, histos) => {
  const result = {...snapshot};
  const availableBTC = snapshot.btc - (0.0025 * snapshot.btc);
  const amountPerCoin =  precisionRound(availableBTC / symToBuy.length, 8);
  for (const toBuy of symToBuy) {
    const histo = histos.find(histo => histo.sym === toBuy.sym);
    const quantity = Math.trunc(amountPerCoin / histo.open);
    const cost = quantity * histo.open + 0.0025 * (quantity * histo.open);
    const wallet = {
      sym : toBuy.sym,
      quantity,
      price: histo.open
    }
    result.btc = precisionRound(result.btc - cost, 8);
    result.wallets.push(wallet);
    result.logs.push(`Buy ${quantity} of ${toBuy.sym} at price ${histo.open}`);
  }
  return result;
}

const valueSnaphot = async(time, snapshot, histos) => {
  let value = snapshot.btc;
  for (const wallet of snapshot.wallets) {
    const histo = histos.find(histo => histo.sym === wallet.sym);
    const walletValue = histo.open * wallet.quantity;
    value = value + walletValue;
  }
  return {...snapshot, estimatedValue : value};
}

const precisionRound = (number, precision) => {
  var factor = Math.pow(10, precision);
  return Math.floor(number * factor) / factor;
}

//scenario(0,5)(1507075200, {btc:1, wallets: []});
exports.scenario = scenario;
