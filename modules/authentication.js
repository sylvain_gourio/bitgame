'use strict';

require('dotenv').config();
const Joi = require('joi');
const Boom = require('boom');
const Randomstring = require('randomstring');
const MemberDAO = require('./dao/member-dao');
const AccountDAO = require('./dao/account-dao');
const Moment = require('moment');
Moment.locale('fr');

const isAdmin = (email) => {
    const admins = process.env.ADMINS;
    return admins && admins.indexOf( email ) >= 0;
};

exports.register = function (server, options, next) {
    return  server.register([require('hapi-auth-cookie'), require('bell')], () => {

        //Setup the session strategy
        server.auth.strategy('session', 'cookie', {
            password: process.env.SESSION_PASSWORD,
            // redirectTo: '/oauth/gplus', //If there is no session, redirect here
            appendNext: true,
            isSecure: process.env.USE_HTTPS //Should be set to true (which is the default) in production
        });

        // Declare an authentication strategy using the bell scheme
        // with the name of the provider, cookie encryption password,
        // and the OAuth client credentials.
        server.auth.strategy('googleplus', 'bell', {
            provider: 'googleplus',
            password: process.env.GPLUS_PASSWORD,
            clientId: process.env.GPLUS_CLIENT_ID,
            clientSecret: process.env.GPLUS_CLIENT_SECRET,
            allowRuntimeProviderParams: true,
            isSecure: false,
            forceHttps: process.env.USE_HTTPS
        });

        // Use the 'twitter' authentication strategy to protect the
        // endpoint handling the incoming authentication credentials.
        // This endpoints usually looks up the third party account in
        // the database and sets some application state (cookie) with
        // the local application account information.
        server.route([{
            method: ['GET', 'POST'], // Must handle both GET and POST
            path: '/oauth/gplus',          // The callback endpoint registered with the provider
            config: {
                auth: 'googleplus',
                description: 'Connect to Bitcoin Trade Game API by Google Plus',
                handler: function (request, reply) {

                    if (!request.auth.isAuthenticated) {
                        return reply('Authentication failed due to: ' + request.auth.error.message);
                    }

                    const email = request.auth.credentials.profile.emails[0].value;
                    const user = {
                        email,
                        displayName: request.auth.credentials.profile.displayName,
                        admin: isAdmin(email)
                    };
                    MemberDAO.findByEmail(email)
                    .then((member) => {
                        if (!member) {
                            const newMember = {
                                email: user.email,
                                displayName: user.displayName,
                                key: Randomstring.generate(8),
                                secret: Randomstring.generate(30),
                            };
                            newMember.creationDate = new Date().toJSON();
                            newMember.creationDateFr = Moment(newMember.creationDate).format('Do MMMM YYYY, HH:mm');
                            if (user.admin) {
                                return AccountDAO.save({
                                    creationDate: newMember.creationDate,
                                    creationDateFr: newMember.creationDateFr,
                                    key: newMember.key,
                                    name: newMember.displayName,
                                    initialBTC: 1,
                                    BTC: 1,
                                    wallets: []
                                })
                                .then(() => MemberDAO.save(newMember))
                                .then(() => {
                                    user.member = true;
                                    user.scope = 'admin';
                                    user.key = newMember.key;
                                });
                            }
                            return AccountDAO.save({
                                creationDate: newMember.creationDate,
                                creationDateFr: newMember.creationDateFr,
                                key: newMember.key,
                                name: newMember.displayName,
                                initialBTC: 1,
                                BTC: 1,
                                wallets: []
                            })
                            .then(() => MemberDAO.save(newMember))
                            .then(() => {
                                user.member = true;
                                user.scope = 'member';
                                user.key = newMember.key;
                            });
                        }
                        if (!member.secret) {
                            member.secret = Randomstring.generate(30);
                            MemberDAO.update(member._id, member);
                        }
                        user.scope = user.admin ? 'admin' : 'member';
                        user.key = member.key;
                    })
                    .then( () => {
                        request.cookieAuth.set(user);
                        // Perform any account lookup or registration, setup local session,
                        // and redirect to the application. The third-party credentials are
                        // stored in request.auth.credentials. Any query parameters from
                        // the initial request are passed back via request.auth.credentials.query.
                        return reply.redirect(request.auth.credentials.query.next || '/');
                    });
                }
            }
        },
        {
            method: 'GET',
            path: '/oauth/user',
            config: {
                description: 'Get connected user if one',
                handler: function (request, reply){
                    const session = request.state.sid;
                    if (!session) {
                        return reply({});
                    }
                    return reply(session);
                },
                cache: {
                    expiresIn: 3 * 1000,
                    privacy: 'private'
                }
            }
        },
        {
            method: 'GET',
            path: '/oauth/logout',
            config: {
                description: 'Logout user',
                handler: (request, reply) => {
                    request.cookieAuth.clear();
                    return reply.redirect('/');
                }
            }
        },
        {
            method: 'POST',
            path: '/oauth/api',
            config: {
                description: 'Connect with apiKey and Secret',
                validate: {
                    payload : {
                        key: Joi.string().required().description('Key of your app'),
                        secret: Joi.string().required().description('Secret for your app')
                    }
                },
                handler: (request, reply) => {
                    const credentials = request.payload;
                    MemberDAO.findByKeyAndSecret(credentials.key, credentials.secret)
                    .then((member) => {
                        if (!member) {
                            return reply(Boom.badRequest());
                        }
                        const user = {
                            email: member.email,
                            displayName: member.displayName,
                            admin: isAdmin(member.email),
                            key: member.key,
                            scope: 'member',
                        };
                        request.cookieAuth.set(user);                  
                        return reply(request.cookieAuth);
                    });
                }
            }
        }]);
        next();
    });
};

exports.register.attributes = {
    name: 'gplusAuthenticationPlugin',
    version: '1.0.0'
};
